# Release notes

[East Scarp](../)

## Version 2.0.0

* Rename from East Scarpe to East Scarp
* Require Stardew 1.5 or higher
* Add East Scarp Obelisk support
* Add rain watering fix
* Redesign all features to be generic and fully configurable
* Remove dependency on TMXL for loading of main East Scarp map

## Version 1.4.0

* Add Crab Pot fishing area support
* Add Orchard fruit tree spawns

## Version 1.1.3

* Avoid additional errors when map fails to load

## Version 1.1.0

* All constants are now in a `data.json` file for easier editing
* Any grass on the map is now preserved through the winter with appropraite sprites

## Version 1.0.0

* Initial version
